#!/usr/bin/env ruby

def read_names(filename)
  File
    .readlines(filename)
    .map { |x| x.split(/\s+/) }
    .to_h
    .transform_keys { |k| k[1..].to_i }
end

def read_pokedex(region)
  File
    .readlines("pokedex_#{region}.txt")
    .map { |x| x.split(/\s+/)[1][1..].to_i }
end

def serialize_dex(region)
  puts <<~EOF
  pub const #{region.upcase}_POKEDEX: &'static [usize] = &[
    0,
  EOF

  read_pokedex(region).each do |n|
    puts "  #{n},"
  end

  puts '];'
end

jp = read_names("pokedex_jp_names.txt")
en = read_names("pokedex_en_names.txt")

puts <<EOF
// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

pub struct Name {
  pub dex: usize,
  pub en: &'static str,
  pub jp: &'static str,
}

pub const NAMES: &'static [Name] = &[
EOF

(1 .. en.size).each do |dex|
  puts "  Name { dex: #{dex}, en: \"#{en[dex]}\", jp: \"#{jp[dex]}\" },"
end

puts "];"

serialize_dex('galar');
serialize_dex('isle_of_armor');
serialize_dex('crown_tundra');
