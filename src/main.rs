// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use std::io::Read;

#[derive(Default)]
struct TestStats {
    failed: usize,
    missing: usize,
    ok: usize,
    total: usize,
}

fn main() {
    if std::env::args().nth(1) == Some("--test".to_string()) {
        let filename = std::env::args().nth(2).unwrap();

        let file = std::fs::File::open(&filename).unwrap();
        let reader = std::io::BufReader::new(file);

        let x: std::collections::HashMap::<String, auto_proofer::Proof> = serde_json::from_reader(reader).unwrap();

        let mut stats = TestStats::default();
        stats.total = x.len();

        for (filename, proof) in x {
            print!("{}", filename);
            if let Ok(mut file) = std::fs::File::open(&filename) {
                let mut bytes = Vec::new();
                file.read_to_end(&mut bytes).unwrap();

                let result = auto_proofer::ocr(&bytes, proof.game());

                let actual = result.value().unwrap();
                if proof == actual {
                    stats.ok += 1;
                    println!(": OK");
                } else {
                    stats.failed += 1;
                    println!(":");
                    println!("  expected: {:?}", proof);
                    println!("  actual:   {:?}", actual);
                }
            } else {
                println!("{}: File not found", filename);
                stats.missing += 1;
            }
        }

        println!("\n\n{} tests, {} ok, {} failed, {} missing", stats.total, stats.ok, stats.failed, stats.missing);
    } else {
        let game     = std::env::args().nth(1).unwrap();
        let filename = std::env::args().nth(2).unwrap();

        println!("game: {}, filename: {}", game, filename);
        let mut file = std::fs::File::open(filename).unwrap();

        let mut bytes = Vec::new();
        file.read_to_end(&mut bytes).unwrap();

        let result = auto_proofer::ocr(&bytes, &game);

        for i in 0..result.log_count() {
            println!("> {}", result.log_entry_message(i));
        }

        println!("{:?}", result.value());
    }
}
