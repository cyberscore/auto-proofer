// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;

pub fn load_image(bytes: &[u8]) -> Result<image::DynamicImage, Box<dyn std::error::Error>> {
    let reader = image::io::Reader::new(std::io::Cursor::new(bytes)).with_guessed_format()?;

    reader.decode().map_err(Into::into)
}

macro_rules! load_image {
    ($filename: expr) => { load_image(include_bytes!($filename)).unwrap() }
}

#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    pub fn log(s: &str);

    #[cfg(any(target_arch = "wasm32", target_arch = "wasm64"))]
    #[wasm_bindgen(js_namespace = console)]
    pub fn time(s: &str);

    #[cfg(any(target_arch = "wasm32", target_arch = "wasm64"))]
    #[wasm_bindgen(js_namespace = console, js_name = timeEnd)]
    pub fn time_end(s: &str);
}

#[cfg(not(any(target_arch = "wasm32", target_arch = "wasm64")))]
pub fn time(s: &str) {
    println!("ts: {}", s);
}

#[cfg(not(any(target_arch = "wasm32", target_arch = "wasm64")))]
pub fn time_end(s: &str) {
    println!("te: {}", s);
}
