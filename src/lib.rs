// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

#[macro_use]
extern crate lazy_static;

extern crate console_error_panic_hook;

use wasm_bindgen::prelude::*;
use serde::{Deserialize, Serialize};

pub mod unionfind;
pub mod characters;
pub mod signatures;
pub mod color;
pub mod utils;
pub mod text;
pub mod ocr;
pub mod pattern;

pub mod pokedex;
pub mod games;

use ocr::{OCRable, Logged};
use games::pokemon_legends_arceus;
use games::pokemon_lets_go;
use games::pokemon_sword_shield;
use games::picross_s4;
use games::new_pokemon_snap;
use games::pokemon_go;
use games::pokemon_scarlet_violet;
use games::kirby_forgotten_land;

#[wasm_bindgen]
#[derive(Clone, Default, Debug, Serialize, Deserialize, PartialEq)]
pub struct Proof {
    #[wasm_bindgen(getter_with_clone)]
    pub pokemon_legends_arceus: Option<pokemon_legends_arceus::Proof>,

    #[wasm_bindgen(getter_with_clone)]
    pub pokemon_lets_go: Option<pokemon_lets_go::Proof>,

    #[wasm_bindgen(getter_with_clone)]
    pub pokemon_sword_shield: Option<pokemon_sword_shield::Proof>,

    #[wasm_bindgen(getter_with_clone)]
    pub picross_s4: Option<picross_s4::Proof>,

    #[wasm_bindgen(getter_with_clone)]
    pub new_pokemon_snap: Option<new_pokemon_snap::Proof>,

    #[wasm_bindgen(getter_with_clone)]
    pub pokemon_go: Option<pokemon_go::Proof>,

    #[wasm_bindgen(getter_with_clone)]
    pub pokemon_scarlet_violet: Option<pokemon_scarlet_violet::Proof>,

    #[wasm_bindgen(getter_with_clone)]
    pub kirby_forgotten_land: Option<kirby_forgotten_land::Proof>,

    pub width: u32,
    pub height: u32,
}

impl Proof {
    pub fn game(&self) -> &str {
        if self.pokemon_legends_arceus.is_some() {
            "pokemon_legends_arceus"
        } else if self.pokemon_lets_go.is_some() {
            "pokemon_lets_go"
        } else if self.pokemon_sword_shield.is_some() {
            "pokemon_sword_shield"
        } else if self.picross_s4.is_some() {
            "picross_s4"
        } else if self.new_pokemon_snap.is_some() {
            "new_pokemon_snap"
        } else if self.pokemon_go.is_some() {
            "pokemon_go"
        } else if self.pokemon_scarlet_violet.is_some() {
            "pokemon_scarlet_violet"
        } else if self.kirby_forgotten_land.is_some() {
            "kirby_forgotten_land"
        } else {
            "unknown"
        }
    }
}


#[wasm_bindgen]
pub fn ocr(proof: &[u8], game: &str) -> ProofResult {
    console_error_panic_hook::set_once();

    OCR::new(game).ocr(proof)
}

#[wasm_bindgen]
#[derive(Default)]
pub struct OCR {
    pokemon_legends_arceus: Option<pokemon_legends_arceus::OCR>,
    pokemon_lets_go: Option<pokemon_lets_go::OCR>,
    pokemon_sword_shield: Option<pokemon_sword_shield::OCR>,
    picross_s4: Option<picross_s4::OCR>,
    new_pokemon_snap: Option<new_pokemon_snap::OCR>,
    pokemon_go: Option<pokemon_go::OCR>,
    pokemon_scarlet_violet: Option<pokemon_scarlet_violet::OCR>,
    kirby_forgotten_land: Option<kirby_forgotten_land::OCR>,
}


#[wasm_bindgen]
pub struct ProofResult {
    result: Logged<Proof>,
}

#[wasm_bindgen]
impl ProofResult {
    pub fn value(&self) -> Option<Proof> {
        self.result.value().clone()
    }

    pub fn log_count(&self) -> usize {
        self.result.log_entries.len()
    }

    pub fn log_entry_message(&self, index: usize) -> String {
        self.result.log_entries[index].message.clone()
    }

    pub fn log_entry_bbox_exists(&self, index: usize) -> bool {
        self.result.log_entries[index].bbox.is_some()
    }

    pub fn log_entry_bbox_x(&self, index: usize) -> Option<usize> {
        self.result.log_entries[index].bbox.clone().map(|bbox| bbox.x)
    }

    pub fn log_entry_bbox_y(&self, index: usize) -> Option<usize> {
        self.result.log_entries[index].bbox.clone().map(|bbox| bbox.y)
    }

    pub fn log_entry_bbox_w(&self, index: usize) -> Option<usize> {
        self.result.log_entries[index].bbox.clone().map(|bbox| bbox.w)
    }

    pub fn log_entry_bbox_h(&self, index: usize) -> Option<usize> {
        self.result.log_entries[index].bbox.clone().map(|bbox| bbox.h)
    }
}

#[wasm_bindgen]
impl OCR {
    #[wasm_bindgen(constructor)]
    pub fn new(game: &str) -> Self {
        let mut result = Self::default();

        match game {
            "pokemon_legends_arceus" => { result.pokemon_legends_arceus = Some(pokemon_legends_arceus::OCR::new()); },
            "pokemon_lets_go"        => { result.pokemon_lets_go        = Some(pokemon_lets_go::OCR::new()); },
            "pokemon_sword_shield"   => { result.pokemon_sword_shield   = Some(pokemon_sword_shield::OCR::new()); },
            "picross_s4"             => { result.picross_s4             = Some(picross_s4::OCR::new()); },
            "new_pokemon_snap"       => { result.new_pokemon_snap       = Some(new_pokemon_snap::OCR::new()); },
            "pokemon_go"             => { result.pokemon_go             = Some(pokemon_go::OCR::new()); },
            "pokemon_scarlet_violet" => { result.pokemon_scarlet_violet = Some(pokemon_scarlet_violet::OCR::new()); },
            "kirby_forgotten_land"   => { result.kirby_forgotten_land   = Some(kirby_forgotten_land::OCR::new()); },
            _ => {},
        }

        result
    }

    #[wasm_bindgen]
    pub fn ocr(&self, proof: &[u8]) -> ProofResult {
        let proof = utils::load_image(proof).ok();
        let proof = match proof {
            Some(p) => p,
            None => { return ProofResult { result: Logged::new() }; },
        };

        if let Some(ocr) = &self.pokemon_legends_arceus {
            return ProofResult { result: ocr.ocr(&proof).map(|p| Proof { pokemon_legends_arceus: Some(p), width: proof.width(), height: proof.height(), ..Default::default() }) };
        }

        if let Some(ocr) = &self.pokemon_lets_go {
            return ProofResult { result: ocr.ocr(&proof).map(|p| Proof { pokemon_lets_go: Some(p), width: proof.width(), height: proof.height(), ..Default::default() }) };
        }

        let mut result = Proof::default();
        result.width = proof.width();
        result.height = proof.height();

        if let Some(ocr) = &self.picross_s4 {
            result.picross_s4 = ocr.ocr(&proof);
            return ProofResult { result: Logged::new().with_value(result) };
        }

        if let Some(ocr) = &self.new_pokemon_snap {
            result.new_pokemon_snap = ocr.ocr(&proof);
            return ProofResult { result: Logged::new().with_value(result) };
        }

        if let Some(ocr) = &self.pokemon_sword_shield {
            return ProofResult { result: ocr.ocr(&proof).map(|p| Proof { pokemon_sword_shield: Some(p), width: proof.width(), height: proof.height(), ..Default::default() }) };
        }

        if let Some(ocr) = &self.pokemon_go {
            return ProofResult { result: ocr.ocr(&proof).map(|p| Proof { pokemon_go: Some(p), width: proof.width(), height: proof.height(), ..Default::default() }) };
        }

        if let Some(ocr) = &self.pokemon_scarlet_violet {
            return ProofResult { result: ocr.ocr(&proof).map(|p| Proof { pokemon_scarlet_violet: Some(p), width: proof.width(), height: proof.height(), ..Default::default() }) };
        }

        if let Some(ocr) = &self.kirby_forgotten_land {
            result.kirby_forgotten_land = ocr.ocr(&proof);
            return ProofResult { result: Logged::new().with_value(result) };
        }

        return ProofResult { result: Logged::new() };
    }
}
