use image::DynamicImage;
use image::{GenericImageView};

#[derive(Debug, Clone, Copy)]
pub struct BoundingBox {
    pub x: usize,
    pub y: usize,
    pub w: usize,
    pub h: usize,
}

impl BoundingBox {
    pub fn new(x: usize, y: usize, w: usize, h: usize) -> Self {
        Self { x, y, w, h }
    }

    pub fn scale(&self, factor: f64) -> Self {
        Self {
            x: (self.x as f64 * factor).floor() as usize,
            y: (self.y as f64 * factor).floor() as usize,
            w: (self.w as f64 * factor).floor() as usize,
            h: (self.h as f64 * factor).floor() as usize,
        }
    }

    pub fn offset(&self, x: usize, y: usize) -> Self {
        Self {
            x: self.x + x,
            y: self.y + y,
            w: self.w,
            h: self.h,
        }
    }

    pub fn extend(&self, x: usize, y: usize) -> Self {
        Self {
            x: self.x.min(x),
            y: self.y.min(y),
            w: (self.x + self.w - 1).max(x) - self.x.min(x) + 1,
            h: (self.y + self.h - 1).max(y) - self.y.min(y) + 1,
        }
    }
}

pub trait HasBBox {
    fn bbox(&self) -> BoundingBox;
}

impl HasBBox for DynamicImage {
    fn bbox(&self) -> BoundingBox {
        let (w, h) = self.dimensions();
        BoundingBox { x: 0, y: 0, w: w as usize, h: h as usize }
    }
}


#[derive(Debug)]
pub struct LogEntry {
    pub message: String,
    pub bbox: Option<BoundingBox>,
}

pub struct Logged<T> {
    t: Option<T>,
    pub log_entries: Vec<LogEntry>,
    factor: f64,
}

impl<T> Logged<T> {
    pub fn new() -> Self {
        Self { t: None, log_entries: vec![], factor: 1.0 }
    }

    pub fn value(&self) -> &Option<T> {
        &self.t
    }

    pub fn with_value(mut self, t: T) -> Self {
        self.t = Some(t);
        self
    }

    pub fn unwrap<F>(self, f: F) -> Option<T> where F: Fn(LogEntry) {
        for entry in self.log_entries {
            f(entry);
        }

        self.t
    }

    pub fn map<U, F>(self, f: F) -> Logged<U> where F: Fn(T) -> U {
        Logged {
            t: self.t.map(f),
            log_entries: self.log_entries,
            factor: self.factor,
        }
    }

}

pub trait OCRable {
    type Proof;

    fn ocr(&self, proof: &image::DynamicImage) -> Logged<Self::Proof>;
}

pub trait Logger {
    fn log(&mut self, message: String, bbox: BoundingBox);
    fn log_plain(&mut self, message: String);
    fn scale_boxes(&mut self, factor: f64);
}

impl<T> Logger for Logged<T> {
    fn log(&mut self, message: String, bbox: BoundingBox) {
        self.log_entries.push(LogEntry { message, bbox: Some(bbox.scale(self.factor)) });
    }

    fn log_plain(&mut self, message: String) {
        self.log_entries.push(LogEntry { message, bbox: None });
    }

    fn scale_boxes(&mut self, factor: f64) {
        self.factor = factor;
    }
}
