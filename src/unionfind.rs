// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

pub struct UnionFind {
    parents: Vec<usize>,
}

impl UnionFind {
    pub fn new(capacity: usize) -> Self {
        let mut s = Self { parents: vec![] };
        s.parents.resize(capacity, 0);
        s
    }

    pub fn insert(&mut self, p: usize) {
        self.parents[p] = p;
    }

    pub fn find(&mut self, p: usize) -> usize {
        let parent = self.parents[p];
        if p != parent {
            let new_parent = self.find(parent);
            self.parents[p] = new_parent;
        }

        self.parents[p]
    }

    pub fn union(&mut self, p: usize, q: usize) {
        let pt = self.find(p);
        let qt = self.find(q);

        if pt != qt {
            self.parents[qt] = pt;
        }
    }

    pub fn flatten(&mut self) {
        for k in 0 .. self.parents.len() {
            self.find(k);
        }
    }
}
