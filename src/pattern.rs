use image::GenericImageView;

pub struct Pattern {
    w: usize,
    h: usize,
    threshold: usize,
    on: Vec<bool>,
    on_count: usize,
}

fn brightness_at(proof: &image::DynamicImage, x: u32, y: u32) -> u32 {
  let pixel = proof.get_pixel(x, y);

  (pixel.0[0] as u32 * 2126 + pixel.0[1] as u32 * 7152 + pixel.0[2] as u32 * 722) / 10000
}

impl Pattern {
    pub fn new(template: &image::DynamicImage, threshold: usize) -> Self {
        let dimensions = template.dimensions();

        let mut on = vec![];
        let mut on_count = 0;

        for y in 0..dimensions.1 {
            for x in 0..dimensions.0 {
                let dark = (brightness_at(template, x, y) as usize) < threshold;
                on.push(dark);
                if dark {
                    on_count += 1;
                }
            }
        }

        Self {
            w: dimensions.0 as usize,
            h: dimensions.1 as usize,
            threshold,
            on,
            on_count,
        }
    }

    pub fn print(&self) {
        for y in 0..self.h {
            for x in 0..self.w {
                print!("{}", if self.on[y*self.w + x] { "x" } else { "." });
            }
            println!("");
        }
        println!("---- {} ----", self.on_count);
    }

    pub fn detect(&self, proof: &image::DynamicImage, x: usize, y: usize, w: usize, h: usize) -> Option<(usize, usize)> {
        let threshold = 100;
        let mut on = vec![];
        let mut on_counts = vec![];

        for yi in 0..h {
            for xi in 0..w {
                let dark = (brightness_at(proof, (x + xi) as u32, (y + yi) as u32) as usize) < self.threshold;
                on.push(dark);
                on_counts.push(if dark { 1 } else { 0 });
            }
        }

        for yi in 1..h {
            for xi in 1..w {
                on_counts[yi*w+xi] += on_counts[(yi-1)*w+xi] + on_counts[yi*w + xi-1] - on_counts[(yi-1)*w + xi - 1];
            }
        }

        let mut attempts = 0;
        let mut best: Option<(usize, usize, usize)> = None;
        for yi in 1..h-self.h {
            for xi in 1..w-self.w {
                let fast_on_count = on_counts[(yi + self.h - 1)*w+(xi + self.w - 1)] + on_counts[(yi - 1)*w+(xi - 1)] - on_counts[(yi - 1)*w+(xi + self.w - 1)] - on_counts[(yi + self.h - 1)*w+(xi - 1)];

                if self.on_count - threshold <= fast_on_count && fast_on_count <= self.on_count + threshold {
                    attempts += 1;
                    let mut diff = 0;
                    for yj in 0..self.h {
                        for xj in 0..self.w {
                            if self.on[yj*self.w+xj] != on[(yi+yj)*w+(xi+xj)] {
                                diff += 1;
                            }
                        }
                    }

                    if diff <= threshold {
                        //println!("{}x{}: {} ({} vs {})", xi, yi, diff, self.on_count, fast_on_count);
                        if best.is_none() || diff < best.unwrap().2 {
                            best = Some((xi, yi, diff));
                        }
                    }
                }
            }
        }

        //println!("full attempts: {}", attempts);
        best.map(|(x,y,_)| (x,y))
    }
}
