// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use image::GenericImageView;
use bit_set::BitSet;
use crate::unionfind::UnionFind;
use crate::ocr::{BoundingBox, HasBBox};

fn brightness_at(proof: &image::DynamicImage, x: u32, y: u32) -> u32 {
  let pixel = proof.get_pixel(x, y);

  (pixel.0[0] as u32 * 2126 + pixel.0[1] as u32 * 7152 + pixel.0[2] as u32 * 722) / 10000
}

#[derive(Debug, Clone)]
pub struct Digit {
    pub bbox: BoundingBox,
    pub pointset: BitSet,
}

impl Digit {
    pub fn print(&self) {
        for y in 0..self.bbox.h {
            for x in 0..self.bbox.w {
                print!("{}", if self.pointset.contains(y*self.bbox.w+x) { "x" } else { "." });
            }
            println!("");
        }
        println!("--------");
    }
}

impl HasBBox for Digit {
    fn bbox(&self) -> BoundingBox {
        self.bbox
    }
}

pub fn detect_characters(proof: &image::DynamicImage, bbox: BoundingBox, threshold: u32) -> Vec<Digit> {
    let mut darkbs = BitSet::new();
    let mut dark = UnionFind::new((bbox.w + 2) * (bbox.h + 2));
    for yi in 0 .. bbox.h {
        for xi in 0 .. bbox.w {
            let b = brightness_at(proof, (bbox.x + xi) as u32, (bbox.y + yi) as u32);
            let is_dark = b < threshold as u32;
            if is_dark {
                dark.insert(yi*bbox.w + xi);
                darkbs.insert(yi*bbox.w + xi);
            }
        }
    }

    for yi in 0 .. bbox.h {
        for xi in 0 .. bbox.w {
            let p = yi*bbox.w + xi;
            if darkbs.contains(p) {
                for dx in -1..=1 {
                    if xi as i32 >= -dx {
                        let tx = (xi as i32 + dx) as usize;
                        for dy in -1..=1 {
                            if yi as i32 >= -dy {
                                let ty = (yi as i32 + dy) as usize;
                                let q = ty*bbox.w + tx;
                                if darkbs.contains(q) {
                                    dark.union(p, q);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    dark.flatten();

    let mut digit_bboxes = std::collections::HashMap::new();
    for yi in 0 .. bbox.h {
        for xi in 0.. bbox.w {
            let idx = yi * bbox.w + xi;
            if darkbs.contains(idx) {
                let idx = dark.find(yi * bbox.w + xi);
                if !digit_bboxes.contains_key(&idx) {
                    digit_bboxes.insert(idx, BoundingBox::new(xi, yi, 1, 1));
                } else {
                    let bbox = digit_bboxes[&idx];
                    digit_bboxes.insert(idx, bbox.extend(xi, yi));
                }
            }
        }
    }

    let mut digits = vec![];
    for (&id, &bb) in digit_bboxes.iter() {
        let mut digit = Digit {
            bbox: bb.offset(bbox.x, bbox.y),
            pointset: BitSet::new(),
        };

        for yi in 0 .. bbox.h {
            for xi in 0 .. bbox.w {
                let idx = yi * bbox.w + xi;
                let is_dark = darkbs.contains(idx);
                if is_dark {
                    let idx = dark.find(idx);
                    if idx == id {
                        let tx = xi - bb.x;
                        let ty = yi - bb.y;
                        digit.pointset.insert(ty * digit.bbox.w + tx);
                    }
                }
            }
        }

        digits.push(digit);
    }

    digits.sort_by_key(|d| (d.bbox.x, d.bbox.y));
    digits
}
