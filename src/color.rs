// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use image::GenericImageView;
use crate::ocr::{BoundingBox, Logger};

pub fn average_color(proof: &image::DynamicImage, x: usize, y: usize, w: usize, h: usize) -> (u8, u8, u8) {
    let mut colorv = (0u32, 0u32, 0u32);
    let mut colorn = 0;

    for yi in y..y+h {
        for xi in x..x+w {
            let pixel = proof.get_pixel(xi as u32, yi as u32).0;
            colorv.0 += pixel[0] as u32;
            colorv.1 += pixel[1] as u32;
            colorv.2 += pixel[2] as u32;
            colorn += 1;
        }
    }

    colorv.0 /= colorn;
    colorv.1 /= colorn;
    colorv.2 /= colorn;

    (colorv.0 as u8, colorv.1 as u8, colorv.2 as u8)
}

pub fn color_diff(a: (u8, u8, u8), b: (u8, u8, u8)) -> u32 {
    a.0.abs_diff(b.0) as u32 + a.1.abs_diff(b.1) as u32 + a.2.abs_diff(b.2) as u32
}

pub fn average_color_diff(proof: &image::DynamicImage, x: usize, y: usize, w: usize, h: usize, expected: (u8, u8, u8)) -> u32 {
    let actual = average_color(proof, x, y, w, h);
    let diff = color_diff(actual, expected);

    diff
}

pub fn average_color_diff_logged(proof: &image::DynamicImage, bbox: BoundingBox, expected: (u8, u8, u8), log: &mut dyn Logger) -> u32 {
    let actual = average_color(proof, bbox.x, bbox.y, bbox.w, bbox.h);
    let diff = color_diff(actual, expected);

    log.log(format!("actual average color: {:?}", actual), bbox.clone());
    log.log(format!("expected average color: {:?}", expected), bbox.clone());
    log.log(format!("average color difference: {}", diff), bbox.clone());

    diff
}


pub fn test_average_color_diff(proof: &image::DynamicImage, bbox: BoundingBox, expected: (u8, u8, u8), threshold: u32, log: &mut dyn Logger) -> bool {
    let diff = average_color_diff_logged(proof, bbox.clone(), expected, log);

    log.log(format!("threshold check: {} < {}", diff, threshold), bbox.clone());

    diff < threshold
}
