pub mod pokemon_legends_arceus;
pub mod pokemon_lets_go;
pub mod pokemon_sword_shield;
pub mod picross_s4;
pub mod new_pokemon_snap;
pub mod pokemon_go;
pub mod pokemon_scarlet_violet;
pub mod kirby_forgotten_land;
