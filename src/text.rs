// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use crate::signatures::Signature;
use crate::characters::{detect_characters, Digit};
use crate::ocr::{BoundingBox, Logger, HasBBox};

pub struct Text {
    signatures: Vec<(usize, char, Signature)>,
    threshold: u32,
}

pub struct Options<'a> {
    pub bbox: Option<BoundingBox>,
    pub height: Option<std::ops::Range<usize>>,
    pub restrict_characters: Option<&'a str>,
    pub log: Option<&'a mut dyn Logger>,
}

impl<'a> Options<'a> {
    pub fn new() -> Self {
        Self { bbox: None, height: None, restrict_characters: None, log: None }
    }
    pub fn bbox(mut self, bbox: BoundingBox) -> Self {
        self.bbox = Some(bbox);
        self
    }

    pub fn height(mut self, height: std::ops::Range<usize>) -> Self {
        self.height = Some(height);
        self
    }

    pub fn chars(mut self, chars: &'a str) -> Self {
        self.restrict_characters = Some(chars);
        self
    }

    pub fn log(mut self, log: &'a mut dyn Logger) -> Self {
        self.log = Some(log);
        self
    }
}

impl Text {
    pub fn new(template: &image::DynamicImage, threshold: u32, characters: &[char]) -> Self {
        let mut signatures = vec![];

        let bbox = template.bbox();
        let w = bbox.w;
        let h = bbox.h / characters.len();

        for index in 0 .. characters.len() {
            for digit in detect_characters(&template, BoundingBox::new(0, h * index, w, h), threshold) {
                signatures.push((index, characters[index], Signature::new(&digit)));
            }
        }

        Self { signatures, threshold }
    }

    pub fn read(&self, image: &image::DynamicImage, mut options: Options) -> String {
        let bbox = options.bbox.unwrap_or(image.bbox());
        let height = options.height.as_ref().unwrap_or(&(0..1000));

        let characters = detect_characters(image, bbox, self.threshold)
            .into_iter()
            .filter(|ch| height.contains(&ch.bbox().h))
            .collect::<Vec<_>>();

        if let Some(log) = options.log.as_mut() {
            for ch in characters.iter() {
                log.log(format!("detected character at {:?}", ch.bbox()), ch.bbox());
            }
        }

        self.match_characters(&characters, options)

    }

    pub fn read_text(&self, image: &image::DynamicImage, x: usize, y: usize, w: usize, h: usize, chars: &str) -> Option<String> {
        Some(self.read(image, Options::new().bbox(BoundingBox::new(x,y,w,h)).chars(chars)))
    }

    pub fn match_characters(&self, characters: &Vec<Digit>, mut options: Options) -> String {
        let chars = options.restrict_characters.unwrap_or("");

        let mut text = String::new();

        for ch in characters {
            let sig = Signature::new(&ch);
            let best = self.match_character(&sig, chars, &mut options);

            if let Some(log) = options.log.as_mut() {
                log.log(format!("digit matches {:?}", best), ch.bbox());
            }
            text.push(best.1);
        }

        if let Some(log) = options.log.as_mut() {
            if let Some(bbox) = options.bbox {
                log.log(format!("read text: {}", text), bbox);
            } else {
                log.log_plain(format!("read text: {}", text));
            }
        }

        text
    }

    pub fn read_text_logged(&self, image: &image::DynamicImage, bbox: BoundingBox, chars: &str, log: &mut dyn Logger) -> String {
        self.read(image, Options::new().bbox(bbox).chars(chars).log(log))
    }

    pub fn read_text_10(&self, image: &image::DynamicImage, x: usize, y: usize, w: usize, h: usize, chars: &str) -> Option<String> {
        let mut characters = detect_characters(image, BoundingBox::new(x, y, w, h), self.threshold);

        characters = characters.iter().filter(|ch| ch.bbox.h >= 15 && ch.bbox.w < 50).cloned().collect::<Vec<_>>();

        characters.sort_by(|a,b| {
            if (a.bbox.y + a.bbox.h).abs_diff(b.bbox.y + b.bbox.h) < a.bbox.h {
                a.bbox.x.cmp(&b.bbox.x)
            } else {
                a.bbox.y.cmp(&b.bbox.y)
            }
        });

        let mut text = String::new();

        let mut previous: Option<BoundingBox> = None;
        for ch in characters {
            if ch.bbox.h >= 15 && ch.bbox.w < 50 {
                ch.print();
                let sig = Signature::new(&ch);
                let best = self.match_character(&sig, chars, &mut Options::new());

                if let Some(bbox) = previous {
                    if ch.bbox.x - (bbox.x + bbox.w) > 10 {
                        text.push(' ');
                    }
                }
                previous = Some(ch.bbox);

                text.push(best.1);
            }
        }

        Some(text)
    }

    pub fn read_usize(&self, image: &image::DynamicImage, mut options: Options) -> Option<usize> {
        let bbox = options.bbox.unwrap_or(image.bbox());

        if let Some(log) = &mut options.log {
            log.log(format!("reading usize"), bbox.clone());
        }

        let text = self.read(image, options);

        let mut number = 0;

        for ch in text.chars() {
            match ch {
                '0' => { number = number * 10 + 0; },
                '1' => { number = number * 10 + 1; },
                '2' => { number = number * 10 + 2; },
                '3' => { number = number * 10 + 3; },
                '4' => { number = number * 10 + 4; },
                '5' => { number = number * 10 + 5; },
                '6' => { number = number * 10 + 6; },
                '7' => { number = number * 10 + 7; },
                '8' => { number = number * 10 + 8; },
                '9' => { number = number * 10 + 9; },
                ',' => {},
                _ => {}
            }
        }

        Some(number)
    }

    pub fn read_usize_unlogged(&self, image: &image::DynamicImage, x: usize, y: usize, w: usize, h: usize, chars: &str) -> Option<usize> {
        let text = self.read_text(image, x, y, w, h, chars)?;
        let mut number = 0;

        for ch in text.chars() {
            match ch {
                '0' => { number = number * 10 + 0; },
                '1' => { number = number * 10 + 1; },
                '2' => { number = number * 10 + 2; },
                '3' => { number = number * 10 + 3; },
                '4' => { number = number * 10 + 4; },
                '5' => { number = number * 10 + 5; },
                '6' => { number = number * 10 + 6; },
                '7' => { number = number * 10 + 7; },
                '8' => { number = number * 10 + 8; },
                '9' => { number = number * 10 + 9; },
                ',' => {},
                _ => {}
            }
        }

        Some(number)
    }

    fn match_character(&self, signature: &Signature, chars: &str, options: &mut Options) -> (usize, char) {
        let mut best = (0, '0', 100000);
        for (i, ch, sig2) in self.signatures.iter() {
            if chars.contains(*ch) {
                let diff = signature.compare(&sig2);

                if let Some(log) = options.log.as_mut() {
                    log.log_plain(format!("match character: {} {} {}", i, ch, diff));
                }
                if diff < best.2 {
                    best = (*i, *ch, diff);
                }
            }
        }

        (best.0, best.1)
    }
}
