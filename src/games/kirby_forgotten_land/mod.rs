// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;
use serde::{Deserialize, Serialize};

//use crate::color::average_color_diff;
use crate::utils::load_image;
//use crate::characters::detect_characters;
use crate::text::Text;


#[wasm_bindgen(js_name = KFLProof)]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Proof {
    #[wasm_bindgen(getter_with_clone)]
    pub coins: Option<CoinsProof>,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct CoinsProof {
    #[wasm_bindgen(getter_with_clone)]
    pub stage: String,
    pub coins: usize,
}

pub struct OCR {
    text: Text,
}

impl OCR {
    pub fn new() -> Self {
        let text = Text::new(
            &load_image(include_bytes!("train-kfl.png")).unwrap(),
            10,
            &"abcdefghijklmnopqrstuvwxyz".chars().collect::<Vec<_>>(), // ♂ is missing
        );

        Self { text }
    }

    pub fn ocr(&self, proof: &image::DynamicImage) -> Option<Proof> {
        let mut inverted = proof.clone();
        inverted.invert();

        let wat = imageproc::geometric_transformations::warp(
            &(inverted.to_rgb8()),
            &imageproc::geometric_transformations::Projection::from_matrix([
                                                                           0.6616, -0.0470, 57.9017,
                                                                          -0.1096,  0.6871, 80.1903,
                                                                          -0.0004, -0.0001,  1.0000,
            ]).unwrap(),
            imageproc::geometric_transformations::Interpolation::Bicubic,
            image::Rgb { 0: [255, 255, 255] },
        );

        wat.save("concreto.png").unwrap();

        let mut result = Proof::default();

        let stage = self.text.read_text_10(&wat.into(), 157, 122, 542, 76, "abcdefghijklmnopqrstuvwxyz");

        println!("{:?}", stage);

        result.coins = Some(CoinsProof { stage: stage?, coins: 0 });

        Some(result)
    }
}

