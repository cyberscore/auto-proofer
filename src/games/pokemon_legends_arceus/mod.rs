// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;
use serde::{Deserialize, Serialize};

use crate::signatures::Signature;
use crate::characters::detect_characters;
use crate::color::{test_average_color_diff, average_color_diff_logged};
use crate::utils::load_image;
use crate::text::{Text, Options};
use crate::ocr::*;

pub enum ProofType {
    Info,
    ResearchTasks,
    PokedexCover,
    LostAndFound,
}

#[wasm_bindgen(js_name = PLAProof)]
#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Proof {
    #[wasm_bindgen(getter_with_clone)] pub research: Option<ResearchTaskProof>,
    pub info: Option<InfoProof>,
    pub lost_and_found: Option<LostAndFoundProof>,
    pub pokedex_cover: Option<PokedexCoverProof>,
}

#[wasm_bindgen]
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct ResearchTaskProof {
    pub dex: usize,
    #[wasm_bindgen(getter_with_clone)] pub tasks: Vec<u32>,
    pub research_level: usize,
}

#[wasm_bindgen]
#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct InfoProof {
    pub dex: usize,
    pub weight_min: u32,
    pub weight_max: u32,
    pub height_min: u32,
    pub height_max: u32,
}

#[wasm_bindgen]
#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct LostAndFoundProof {
    pub total_mp: usize,
    pub retrieved_dropped_items: usize,
}

#[wasm_bindgen]
#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct PokedexCoverProof {
    pub total_seen: usize,
    pub total_caught: usize,
    pub fieldlands_seen: Option<usize>,
    pub fieldlands_caught: Option<usize>,
    pub mirelands_seen: Option<usize>,
    pub mirelands_caught: Option<usize>,
    pub coastlands_seen: Option<usize>,
    pub coastlands_caught: Option<usize>,
    pub highlands_seen: Option<usize>,
    pub highlands_caught: Option<usize>,
    pub icelands_seen: Option<usize>,
    pub icelands_caught: Option<usize>,
    pub research_points: usize,
}

const TOPLEFTS: [(usize, usize); 5] = [
    (1070, 171),
    (1070, 264),
    (1070, 357),
    (1070, 450),
    (1070, 543),
];

pub struct OCR {
    selected_page_template: image::DynamicImage,
    training: Text,
    training_info: Text,
}

impl OCRable for OCR {
    type Proof = Proof;

    fn ocr(&self, image: &image::DynamicImage) -> Logged<Proof> {
        let mut result = Logged::new();

        let mut proof = Proof::default();

        match self.proof_type(&image, &mut result) {
            Some(ProofType::ResearchTasks) => { proof.research       = self.research(&image, &mut result); },
            Some(ProofType::Info)          => { proof.info           = self.info(&image, &mut result); },
            Some(ProofType::LostAndFound)  => { proof.lost_and_found = self.lost_and_found(&image, &mut result); },
            Some(ProofType::PokedexCover)  => { proof.pokedex_cover  = self.pokedex_cover(&image, &mut result); },
            None => { result.log_plain(format!("couldn't detect out proof type")); },
        }

        result.with_value(proof)
    }
}

impl OCR {
    pub fn new() -> Self {
        let selected_page_template = load_image(include_bytes!("selected-page-template.png")).unwrap();
        let training      = load_image(include_bytes!("train-digits.png" )).unwrap();
        let training_info = load_image(include_bytes!("train-info.png" )).unwrap();

        Self {
            selected_page_template,
            training: Text::new(&training, 128, &['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ',']),
            training_info: Text::new(&training_info, 160, &['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', 'l', 'b', 's', '-', '\'', ',', 'x', 'x', 'x']),
        }
    }

    pub fn selected_index(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> Option<usize> {
        let mut best = (6, 0x3FFFFFFF);
        for (i, &(x, y)) in TOPLEFTS.iter().enumerate() {
            let diff = average_color_diff_logged(image, BoundingBox::new(x, y - 12, 40, 12), (255, 255, 255), log);
            if diff < best.1 {
                best = (i, diff);
            }
        }

        if best.0 == 6 {
            log.log_plain(format!("Selected index: None!"));
            None
        } else {
            log.log_plain(format!("Selected index: {}", best.0));
            Some(best.0)
        }
    }

    pub fn dex_number(&self, proof: &image::DynamicImage, index: usize, log: &mut dyn Logger) -> Option<usize> {
        self.find_number(proof, BoundingBox::new(TOPLEFTS[index].0, TOPLEFTS[index].1, 40, 20), log)
    }

    pub fn is_cover(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> bool {
        log.log_plain(format!("proof-type: is cover?"));
        let c0 = test_average_color_diff(proof, BoundingBox::new(474, 129, 56, 89), (239, 235, 213), 30, log);
        let c1 = test_average_color_diff(proof, BoundingBox::new(850, 129, 70, 89), (239, 235, 213), 30, log);
        let c2 = test_average_color_diff(proof, BoundingBox::new(630, 557, 200, 60), (37, 58, 81), 30, log);

        c0 && c1 && c2
    }

    pub fn is_lost_and_found(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> bool {
        log.log_plain(format!("proof-type: is lost and found?"));
        let c0 = test_average_color_diff(proof, BoundingBox::new(239, 244, 338, 26), (51, 78, 101), 30, log);
        let c1 = test_average_color_diff(proof, BoundingBox::new(700, 244, 338, 26), (51, 78, 101), 30, log);
        let c2 = test_average_color_diff(proof, BoundingBox::new(258, 284, 770, 54), (239, 235, 213), 30, log);

        c0 && c1 && c2
    }

    pub fn proof_type(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> Option<ProofType> {
        if self.is_cover(proof, log) {
            log.log_plain(format!("proof-type: is cover"));
            return Some(ProofType::PokedexCover);
        }

        if self.is_lost_and_found(proof, log) {
            log.log_plain(format!("proof-type: is lost and found"));
            return Some(ProofType::LostAndFound);
        }

        let selected_page_chars = detect_characters(&self.selected_page_template, BoundingBox::new(0, 0, 12, 13), 128);
        if selected_page_chars.is_empty() {
            return None;
        }

        let signature = Signature::new(&selected_page_chars[0]);

        let chars0 = detect_characters(proof,  BoundingBox::new(54, 22, 12, 13), 128);
        if chars0.is_empty() {
            return None;
        }

        let chars1 = detect_characters(proof, BoundingBox::new(101, 22, 12, 13), 128);
        if chars1.is_empty() {
            return None;
        }

        let p0 = signature.compare(&Signature::new(&chars0[0]));
        let p1 = signature.compare(&Signature::new(&chars1[0]));

        if p0 < p1 {
            Some(ProofType::Info)
        } else {
            Some(ProofType::ResearchTasks)
        }
    }

    pub fn research(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> Option<ResearchTaskProof> {
        let index = self.selected_index(proof, log)?;
        let dex = self.dex_number(proof, index, log)?;
        let research_level = self.find_number(proof, BoundingBox::new(822, 572, 60, 35), log)?;

        let perfect = test_average_color_diff(proof, BoundingBox::new(683, 578, 4, 4), (233, 226, 103), 30, log);

        let mut rtp = ResearchTaskProof {
            dex,
            tasks: vec![],
            research_level: if perfect && research_level == 10 { 11 } else { research_level },
        };

        for i in 0 ..= 8 {
            let task = self.find_number(proof, BoundingBox::new(520, 144 + i*39, 50, 20), log);
            if task.is_some() {
                rtp.tasks.push(task.unwrap() as u32);
            }
        }

        Some(rtp)
    }

    pub fn info(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> Option<InfoProof> {
        let index = self.selected_index(proof, log)?;
        let dex = self.dex_number(proof, index, log)?;

        let (weight_min, weight_max) = self.weight(proof, log)?;
        let (height_min, height_max) = self.height(proof, log)?;

        Some(InfoProof {
            dex,
            weight_min,
            weight_max,
            height_min,
            height_max,
        })
    }

    pub fn lost_and_found(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> Option<LostAndFoundProof> {
        let total_mp = self.find_number(proof, BoundingBox::new(753, 334, 246, 32), log)?;
        let retrieved_dropped_items = self.find_number(proof, BoundingBox::new(753, 376, 246, 32), log)?;

        Some(LostAndFoundProof {
            total_mp,
            retrieved_dropped_items,
        })
    }

    pub fn pokedex_cover(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> Option<PokedexCoverProof> {
        let mut inverted = proof.clone();
        inverted.invert();

        let fieldlands_active = test_average_color_diff(proof, BoundingBox::new(328, 387, 30, 30), (233, 231, 220), 30, log);
        let mirelands_active  = test_average_color_diff(proof, BoundingBox::new(585, 387, 30, 30), (233, 231, 220), 30, log);
        let coastlands_active = test_average_color_diff(proof, BoundingBox::new(842, 387, 30, 30), (233, 231, 220), 30, log);
        let highlands_active  = test_average_color_diff(proof, BoundingBox::new(454, 469, 30, 30), (233, 231, 220), 30, log);
        let icelands_active   = test_average_color_diff(proof, BoundingBox::new(712, 469, 30, 30), (233, 231, 220), 30, log);

        let has_tenth_rank = test_average_color_diff(proof, BoundingBox::new(584, 597, 7, 7), (251, 244, 217), 30, log);

        let research_points = if has_tenth_rank {
            self.find_number(&inverted, BoundingBox::new(962, 576, 91, 22), log)?
        } else {
            self.find_number(&inverted, BoundingBox::new(967, 554, 86, 25), log)?
        };

        Some(PokedexCoverProof {
            total_seen:        self.find_number(&inverted, BoundingBox::new(611, 277, 58, 30), log)?,
            total_caught:      self.find_number(&inverted, BoundingBox::new(749, 277, 58, 30), log)?,
            fieldlands_seen:   if fieldlands_active { Some(self.find_number(proof, BoundingBox::new(383, 391, 36, 24), log)?) } else { None },
            fieldlands_caught: if fieldlands_active { Some(self.find_number(proof, BoundingBox::new(466, 391, 36, 24), log)?) } else { None },
            mirelands_seen:    if  mirelands_active { Some(self.find_number(proof, BoundingBox::new(641, 391, 36, 24), log)?) } else { None },
            mirelands_caught:  if  mirelands_active { Some(self.find_number(proof, BoundingBox::new(724, 391, 36, 24), log)?) } else { None },
            coastlands_seen:   if coastlands_active { Some(self.find_number(proof, BoundingBox::new(897, 391, 36, 24), log)?) } else { None },
            coastlands_caught: if coastlands_active { Some(self.find_number(proof, BoundingBox::new(979, 391, 36, 24), log)?) } else { None },
            highlands_seen:    if  highlands_active { Some(self.find_number(proof, BoundingBox::new(507, 473, 36, 24), log)?) } else { None },
            highlands_caught:  if  highlands_active { Some(self.find_number(proof, BoundingBox::new(589, 473, 36, 24), log)?) } else { None },
            icelands_seen:     if   icelands_active { Some(self.find_number(proof, BoundingBox::new(765, 473, 36, 24), log)?) } else { None },
            icelands_caught:   if   icelands_active { Some(self.find_number(proof, BoundingBox::new(848, 473, 36, 24), log)?) } else { None },
            research_points,
        })
    }

    pub fn weight(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> Option<(u32, u32)> {
        let re = regex::Regex::new(r#"^([\d,.]+)lbs\.(?:-([\d,.]+)lbs\.)?$"#).unwrap();

        let text = self.training_info.read_text_logged(proof, BoundingBox::new(640, 153, 180, 20), "0123456789'-.,lbskgm", log);
        let text = text.replace(',', "");

        let result = re.captures(&text)?;
        if result.get(2).is_some() {
            let value0 = result.get(1).map(|m| m.as_str()).unwrap().parse::<f64>().ok()?;
            let value1 = result.get(2).map(|m| m.as_str()).unwrap().parse::<f64>().ok()?;
            log.log_plain(format!("found two weights: {} | {}", value0, value1));

            let value0 = (value0 * 10.0) as u32;
            let value1 = (value1 * 10.0) as u32;

            log.log_plain(format!("converting to centipounds: {} | {}", value0, value1));
            Some((value0, value1))
        } else {
            let value = result.get(1).map(|m| m.as_str()).unwrap().parse::<f64>().ok()?;
            log.log_plain(format!("found one weight: {}", value));

            let value = (value * 10.0) as u32;
            log.log_plain(format!("converting to centipounds: {}", value));
            Some((value, value))
        }
    }

    pub fn height(&self, proof: &image::DynamicImage, log: &mut dyn Logger) -> Option<(u32, u32)> {
        let re = regex::Regex::new(r#"^(?:(\d+)')?(?:(\d+)'')?(?:-(?:(\d+)')?(?:(\d+)'')?)?$"#).unwrap();

        let text = self.training_info.read_text_logged(proof, BoundingBox::new(640, 190, 180, 18), "0123456789'-.,lbskgm", log);

        let result = re.captures(&text)?;

        if result.get(3).is_some() || result.get(4).is_some() {
            let feet0 = result.get(1).map(|m| m.as_str()).unwrap_or("0").parse::<u32>().ok()?;
            let inch0 = result.get(2).map(|m| m.as_str()).unwrap_or("0").parse::<u32>().ok()?;
            let feet1 = result.get(3).map(|m| m.as_str()).unwrap_or("0").parse::<u32>().ok()?;
            let inch1 = result.get(4).map(|m| m.as_str()).unwrap_or("0").parse::<u32>().ok()?;

            log.log_plain(format!("found two heights: {} {} | {} {}", feet0, inch0, feet1, inch1));
            log.log_plain(format!("converting to inches: {} | {}", feet0 * 12 + inch0, feet1 * 12 + inch1));
            Some((feet0*12 + inch0, feet1*12 + inch1))
        } else {
            let feet = result.get(1).map(|m| m.as_str()).unwrap_or("0").parse::<u32>().ok()?;
            let inch = result.get(2).map(|m| m.as_str()).unwrap_or("0").parse::<u32>().ok()?;

            let value = feet*12 + inch;

            log.log_plain(format!("found one height: {} {}", feet, inch));
            log.log_plain(format!("converting to inches: {}", value));
            Some((value, value))
        }
    }

    fn find_number(&self, proof: &image::DynamicImage, bbox: BoundingBox, log: &mut dyn Logger) -> Option<usize> {
        self.training.read_usize(proof, Options::new().bbox(bbox).chars("0123456789,.").log(log))
    }
}

pub fn ocr(proof: &image::DynamicImage) -> Logged<Proof> {
    OCR::new().ocr(proof)
}
