// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;
use image::GenericImageView;
use serde::{Deserialize, Serialize};

use crate::utils::load_image;
use crate::text::{Text, Options};
use crate::characters::detect_characters;
use crate::ocr::*;

#[wasm_bindgen(js_name = POGOProof)]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Proof {
    pub pokedex_entry: Option<PokedexEntryProof>,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct PokedexEntryProof {
    pub dex: usize,
    pub seen: Option<usize>,
    pub caught: Option<usize>,
    pub lucky: Option<usize>,
    pub purified: Option<usize>,
    pub weight_min: Option<usize>,
    pub weight_max: Option<usize>,
    pub height_min: Option<usize>,
    pub height_max: Option<usize>,
}

pub struct OCR {
    text: Text,
    text2: Text,
    smol_nonlucky: Text,
}

impl OCRable for OCR {
    type Proof = Proof;
    fn ocr(&self, image: &image::DynamicImage) -> Logged<Proof> {
        let mut result = Logged::new();

        let mut proof = Proof::default();

        proof.pokedex_entry = Some(self.ocr_pokedex_entry(image, &mut result));

        result.with_value(proof)
    }
}

impl OCR {
    pub fn new() -> Self {
        let text = Text::new(
            &load_image(include_bytes!("train-pogo-dex.png")).unwrap(),
            20,
            &"0123456789.".chars().collect::<Vec<_>>(),
        );

        let text2 = Text::new(
            &load_image(include_bytes!("train-pogo-dex-smol.png")).unwrap(),
            40,
            &"0123456789x.F".chars().collect::<Vec<_>>(),
        );

        let smol_nonlucky = Text::new(
            &load_image(include_bytes!("train-pogo-dex-smol.png")).unwrap(),
            30,
            &"0123456789x.F".chars().collect::<Vec<_>>(),
        );

        Self { text, text2, smol_nonlucky }
    }

    pub fn ocr_pokedex_entry(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> PokedexEntryProof {
        let mut normalized = image.resize(720, 5000, image::imageops::FilterType::Lanczos3);
        normalized.invert();

        log.scale_boxes(image.dimensions().0 as f64 / 720.0);

        let mut proof = PokedexEntryProof::default();

        let b = BoundingBox::new(160, 1, normalized.bbox().w - 2*260, normalized.bbox().h - 100);
        log.log(format!("finding dex number text"), b);
        let ys = detect_characters(&normalized, b, 20)
            .into_iter()
            .filter(|ch| (37..44).contains(&ch.bbox.h))
            .collect::<Vec<_>>();

        for ch in ys.iter() {
            log.log(format!("found character: {:?}", ch.bbox), ch.bbox);
        }

        let mut ys = ys.into_iter().map(|ch| ch.bbox().y).collect::<Vec<_>>();
        ys.sort();

        if ys.is_empty() {
            log.log_plain(format!("no characters found"));
            return proof;
        }

        let dex_y = ys[ys.len() / 2];

        log.log_plain(format!("min y: {:?}", dex_y));

        let dex_bbox = BoundingBox::new(1, dex_y - 10, normalized.dimensions().0 as usize - 2, 60);
        let dex = self.text.read(&normalized, Options::new().bbox(dex_bbox).chars("0123456789").height(33..48).log(log));
        if dex.len() < 4 {
            log.log_plain(format!("dex string too short: {:?}", dex));
            return proof;
        }
        let dex = &dex[0..4];
        log.log(format!("dex: {:?}", dex), dex_bbox);

        proof.dex = match dex.parse::<usize>().ok() {
            Some(n) => n,
            None => { log.log_plain(format!("couldn't read dex entry")); return proof; },
        };

        let counts = self.text2
            .read(&normalized, Options::new().bbox(BoundingBox::new(1, dex_y + 70, 718, 40)).chars("0123456789x.").height(16..27).log(log))
            .replace("x", " ")
            .replace(".", " ");

        let counts = counts.split_whitespace().collect::<Vec<_>>();

        log.log_plain(format!("counts: {:?}", counts));

        if counts.len() < 2 {
            log.log_plain(format!("didn't find at least two counts: {:?}", counts));
            return proof;
        }

        proof.seen = counts[0].parse::<usize>().ok();
        proof.caught = counts[1].parse::<usize>().ok();
        if counts.len() > 2 {
            proof.lucky = counts[2].parse::<usize>().ok();
        }
        if counts.len() > 3 {
            proof.purified = counts[3].parse::<usize>().ok();
        }

        proof.weight_min = self.dimension(&normalized, BoundingBox::new( 40, dex_y + 305, 150, 30), proof.lucky.unwrap_or(0) > 0, log);
        proof.weight_max = self.dimension(&normalized, BoundingBox::new(195, dex_y + 305, 150, 30), proof.lucky.unwrap_or(0) > 0, log);
        proof.height_min = self.dimension(&normalized, BoundingBox::new(385, dex_y + 305, 150, 30), proof.lucky.unwrap_or(0) > 0, log);
        proof.height_max = self.dimension(&normalized, BoundingBox::new(540, dex_y + 305, 150, 30), proof.lucky.unwrap_or(0) > 0, log);

        log.log_plain(format!("weights min: {:?}", proof.weight_min));
        log.log_plain(format!("weights max: {:?}", proof.weight_max));
        log.log_plain(format!("heights min: {:?}", proof.height_min));
        log.log_plain(format!("heights max: {:?}", proof.height_max));

        proof
    }

    pub fn dimension(&self, normalized: &image::DynamicImage, bbox: BoundingBox, lucky: bool, log: &mut dyn Logger) -> Option<usize> {
        if lucky { &self.text2 } else { &self.smol_nonlucky }
            .read(normalized, Options::new().bbox(bbox).chars("0123456789x.F").height(2..27).log(log))
                .replace("x", "")
                .replace("F", "44")
                .parse::<f64>()
                .ok()
                .map(|x| (x * 100f64).round() as usize)
    }
}
