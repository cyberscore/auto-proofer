// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;
use serde::{Deserialize, Serialize};

use crate::color::test_average_color_diff;
use crate::utils::load_image;
use crate::text::{Text, Options};
use crate::pokedex::NAMES;
use crate::pokedex::GALAR_POKEDEX;
use crate::pokedex::CROWN_TUNDRA_POKEDEX;
use crate::pokedex::ISLE_OF_ARMOR_POKEDEX;
use crate::ocr::*;

#[wasm_bindgen(js_name = PSWSHProof)]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Proof {
    pub league: Option<LeagueProof>,
    #[wasm_bindgen(getter_with_clone)]
    pub pokedex: Option<PokedexProof>,
    pub cover: Option<CoverProof>,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct LeagueProof {
    pub curry_dex: usize,
    pub best_rally_score: usize,
    pub pokemon_caught: usize,
    pub shiny_pokemon_found: usize,
}

#[wasm_bindgen(js_name = PSWSHPokedexProof)]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct PokedexProof {
    pub dex: usize,
    #[wasm_bindgen(getter_with_clone)]
    pub region: String,
    pub number_battled: usize,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct CoverProof {
    pub galar_caught: usize,
    pub galar_seen: usize,
    pub isle_of_armor_caught: usize,
    pub isle_of_armor_seen: usize,
    pub crown_tundra_caught: usize,
    pub crown_tundra_seen: usize,
}

pub struct OCR {
    text: Text,
    name: Text,
}

const NAME_CHARS: &str = "abcdefghijklmnopqrstuvwxyz.'-ヤトキンクスチルレ♀♂アーケオポホコリ2ロフタカラサハ°テネモャヒナッメマダムイセヘプィウュォツミ__";

fn find_pokemon(name: &str) -> Option<(&str, usize)> {
    let lname = name.replace("i", "l");
    for entry in NAMES.iter() {
        if entry.en.replace("i", "l") == lname || entry.jp == name {
            let galar = GALAR_POKEDEX.iter().position(|x| *x == entry.dex);
            let isle_of_armor = ISLE_OF_ARMOR_POKEDEX.iter().position(|x| *x == entry.dex);
            let crown_tundra = CROWN_TUNDRA_POKEDEX.iter().position(|x| *x == entry.dex);

            return galar.map(|x| ("galar", x))
                .or(isle_of_armor.map(|x| ("isle_of_armor", x)))
                .or(crown_tundra.map(|x| ("crown_tundra", x)));
        }
    }

    None
}

fn japanese_shenanigans(text: &String) -> String {
    text
        .replace("j.", "j")
        .replace(".i", "i")
        .replace("ト''", "ド")
        .replace("ク'", "グ")
        .replace("--ン", "シ")
        .replace("シ''", "ジ")
        .replace("-ン", "ン")
        .replace("ン-", "ン")
        .replace("ルル", "ル")
        .replace("ホポホ", "ポ")
        .replace("ホホホ", "ホ")
        .replace("コ'", "ゴ")
        .replace("リリ", "リ")
        .replace("フ'", "ブ")
        .replace("カ''", "ガ")
        .replace("ラー", "ラ")
        .replace("ハハ", "ハ")
        .replace("ハ°", "パ")
        .replace("テー", "テ")
        .replace("テ'", "デ")
        .replace("ネ-", "ネ")
        .replace("ヒ°", "ピ")
        .replace("ッッッ", "ッ")
        .replace("ツツツ", "ツ")
        .replace("サ''", "ザ")
        .replace("ヒ''", "ビ")
        .replace("セ''", "ゼ")
        .replace("ヘ°", "ペ")
        .replace("ハ''", "バ")
        .replace("ーー", "ニ")
        .replace("キ''", "ギ")
        .replace("ヘ''", "ベ")
        .replace("ケ''", "ゲ")
        .replace("ミミミ", "ミ")
}

impl OCR {
    pub fn new() -> Self {
        let text = Text::new(
            &load_image(include_bytes!("train-swsh.png")).unwrap(),
            100,
            &"0123456789,x".chars().collect::<Vec<_>>(),
        );

        let name = Text::new(
            &load_image(include_bytes!("train-swsh-names.png")).unwrap(),
            100,
            &NAME_CHARS.chars().collect::<Vec<_>>(),
        );

        Self { text, name }
    }

    pub fn ocr_cover(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> Option<CoverProof> {
        let mut proof = CoverProof::default();

        let mut inverted = image.clone();
        inverted.invert();

        let has_isle_of_armor = test_average_color_diff(image, BoundingBox::new(802, 238, 10, 10), (0, 161, 138), 20, log);
        let has_crown_tundra = test_average_color_diff(image, BoundingBox::new(802, 138, 10, 10), (249, 223, 6), 20, log);

        log.log_plain(format!("DLCs: isle of armor: {}, crown tundra: {}", has_isle_of_armor, has_crown_tundra));

        if !has_crown_tundra && !has_isle_of_armor {
            proof.galar_caught = self.text.read_usize(&inverted, Options::new().bbox(BoundingBox::new( 958, 184, 56, 33)).chars("0123456789x").log(log))?;
            proof.galar_seen   = self.text.read_usize(&inverted, Options::new().bbox(BoundingBox::new(1171, 184, 56, 33)).chars("0123456789x").log(log))?;
        } else {
            let galar_selected  = test_average_color_diff(image, BoundingBox::new(830,  30, 33, 33), (0,0,0), 20, log);
            let tundra_selected = test_average_color_diff(image, BoundingBox::new(830, 127, 33, 33), (0,0,0), 20, log);
            let isle_selected   = test_average_color_diff(image, BoundingBox::new(830, 226, 33, 33), (0,0,0), 20, log);

            proof.galar_caught = self.text.read_usize(if galar_selected { &inverted } else { image }, Options::new().bbox(BoundingBox::new( 958, 70, 56, 33)).chars("0123456789x").log(log))?;
            proof.galar_seen   = self.text.read_usize(if galar_selected { &inverted } else { image }, Options::new().bbox(BoundingBox::new(1171, 70, 56, 33)).chars("0123456789x").log(log))?;

            proof.isle_of_armor_caught = self.text.read_usize(if tundra_selected { &inverted } else { image }, Options::new().bbox(BoundingBox::new( 958, 169, 56, 33)).chars("0123456789x").log(log))?;
            proof.isle_of_armor_seen   = self.text.read_usize(if tundra_selected { &inverted } else { image }, Options::new().bbox(BoundingBox::new(1171, 169, 56, 33)).chars("0123456789x").log(log))?;

            proof.crown_tundra_caught = self.text.read_usize(if isle_selected { &inverted } else { image }, Options::new().bbox(BoundingBox::new( 958, 267, 56, 33)).chars("0123456789x").log(log))?;
            proof.crown_tundra_seen   = self.text.read_usize(if isle_selected { &inverted } else { image }, Options::new().bbox(BoundingBox::new(1171, 267, 56, 33)).chars("0123456789x").log(log))?;
        }

        Some(proof)
    }

    pub fn ocr_league_card(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> LeagueProof {
        let mut proof = LeagueProof::default();
        let mut inverted = image.clone();
        inverted.invert();

        proof.curry_dex =           self.text.read_usize(&inverted, Options::new().bbox(BoundingBox::new(366, 277, 243, 33)).chars("0123456789,x").log(log)).unwrap_or(0);
        proof.best_rally_score =    self.text.read_usize(&inverted, Options::new().bbox(BoundingBox::new(366, 356, 243, 32)).chars("0123456789,x").log(log)).unwrap_or(0);
        proof.pokemon_caught =      self.text.read_usize(&inverted, Options::new().bbox(BoundingBox::new(366, 432, 243, 33)).chars("0123456789,x").log(log)).unwrap_or(0);
        proof.shiny_pokemon_found = self.text.read_usize(&inverted, Options::new().bbox(BoundingBox::new(366, 512, 243, 32)).chars("0123456789,x").log(log)).unwrap_or(0);

        proof
    }

    pub fn ocr_pokedex(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> Option<PokedexProof> {
        let mut proof = PokedexProof::default();
        let mut inverted = image.clone();
        inverted.invert();

        let name = japanese_shenanigans(&self.name.read(&inverted, Options::new().bbox(BoundingBox::new(823, 95, 266, 60)).chars(NAME_CHARS).log(log)));
        let entry = find_pokemon(&name)?;

        proof.region = entry.0.to_string();
        proof.dex = entry.1;
        proof.number_battled = self.text.read_usize(image, Options::new().bbox(BoundingBox::new(882, 423, 250, 50)).chars("0123456789,x").log(log))?;

        Some(proof)
    }
}

impl OCRable for OCR {
    type Proof = Proof;
    fn ocr(&self, image: &image::DynamicImage) -> Logged<Proof> {
        let mut result = Logged::new();

        let mut proof = Proof::default();

        let league_bbox = BoundingBox::new(0, 0, 50, 50);
        let is_league_card = test_average_color_diff(image, league_bbox, (67, 167, 255), 20, &mut result);
        let pokedex_bbox = BoundingBox::new(1229, 97, 7, 58);
        let is_pokedex     = test_average_color_diff(image, pokedex_bbox, (0, 0, 0), 20, &mut result);
        let is_cover       = !is_league_card && !is_pokedex;

        result.log(format!("is league? {}", is_league_card), league_bbox);
        result.log(format!("is pokedex? {}", is_pokedex), pokedex_bbox);
        result.log_plain(format!("is cover? {}", is_cover));

        if is_league_card {
            proof.league = Some(self.ocr_league_card(image, &mut result));
        } else if is_pokedex {
            proof.pokedex = self.ocr_pokedex(image, &mut result);
        } else {
            proof.cover = self.ocr_cover(image, &mut result);
        }

        result.with_value(proof)
    }
}
