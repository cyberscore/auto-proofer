// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;
use serde::{Deserialize, Serialize};

use crate::utils::load_image;
use crate::text::{Text, Options};
use crate::color::test_average_color_diff;
use crate::ocr::*;

#[wasm_bindgen(js_name = PScaVioProof)]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Proof {
    pub profile: Option<ProfileProof>,
    #[wasm_bindgen(getter_with_clone)]
    pub sandwich: Option<SandwichProof>,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct ProfileProof {
    pub badges_collected: usize,
    pub pokedex_seen: usize,
    pub pokedex_caught: usize,
    pub shiny_pokemon_battled: usize,
    pub recipes_collected: usize,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct SandwichProof {
    #[wasm_bindgen(getter_with_clone)]
    pub name: String,
    pub times_made: usize,
}

pub struct OCR {
    profile: Text,
    sandwich_id: Text,
    sandwich_n: Text,
}

impl OCR {
    pub fn new() -> Self {
        let profile = Text::new(
            &load_image(include_bytes!("profile.png")).unwrap(),
            40,
            &"0123456789xxx".chars().collect::<Vec<_>>(),
        );

        let sandwich_id = Text::new(
            &load_image(include_bytes!("sandwiches.png")).unwrap(),
            230,
            &"0123456789xxx".chars().collect::<Vec<_>>(),
        );

        let sandwich_n = Text::new(
            &load_image(include_bytes!("sandwich_n.png")).unwrap(),
            80,
            &"0123456789xxx".chars().collect::<Vec<_>>(),
        );

        Self { profile, sandwich_id, sandwich_n }
    }

    pub fn ocr_profile(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> ProfileProof {
        let mut proof = ProfileProof::default();

        let mut inverted = image.clone();
        inverted.invert();

        let has_19_badges = test_average_color_diff(image, BoundingBox::new(186, 557, 2, 2), (193, 164, 122), 20, log);

        if has_19_badges {
            proof.badges_collected = 19;
        } else {
            proof.badges_collected = self.profile.read_usize(&inverted, Options::new().bbox(BoundingBox::new(1040, 224, 50, 30)).chars("0123456789xxx").log(log)).unwrap_or(0);
        }

        proof.pokedex_caught = self.profile.read_usize(&inverted, Options::new().bbox(BoundingBox::new(916, 305, 50, 30)).chars("0123456789xxx").log(log)).unwrap_or(0);
        proof.pokedex_seen = self.profile.read_usize(&inverted, Options::new().bbox(BoundingBox::new(1036, 305, 50, 30)).chars("0123456789xxx").log(log)).unwrap_or(0);
        proof.shiny_pokemon_battled = self.profile.read_usize(&inverted, Options::new().bbox(BoundingBox::new(1036, 386, 50, 30)).chars("0123456789xxx").log(log)).unwrap_or(0);
        proof.recipes_collected = self.profile.read_usize(&inverted, Options::new().bbox(BoundingBox::new(1036, 465, 50, 30)).chars("0123456789xxx").log(log)).unwrap_or(0);

        proof
    }

    pub fn ocr_sandwich(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> SandwichProof {
        let mut proof = SandwichProof::default();

        let number_bbox = BoundingBox::new(1156, 148, 106, 41);
        let number = self.sandwich_id.read_usize(image, Options::new().bbox(number_bbox).chars("0123456789").log(log)).unwrap_or(0);
        log.log(format!("Sandwich ID: {}", number), number_bbox);

        if 0 < number && number <= 151 {
            proof.name = NAMES[number - 1].to_string();
        }

        proof.times_made = self.sandwich_n.read_usize(image, Options::new().bbox(BoundingBox::new(724, 362, 136, 35)).chars("0123456789").log(log)).unwrap_or(42);

        proof
    }
}

impl OCRable for OCR {
    type Proof = Proof;
    fn ocr(&self, image: &image::DynamicImage) -> Logged<Proof> {
        let mut result = Logged::new();

        let mut proof = Proof::default();

        let is_sandwich = test_average_color_diff(image, BoundingBox::new(717, 104, 20, 16), (83, 154, 60), 20, &mut result);

        if is_sandwich {
            proof.sandwich = Some(self.ocr_sandwich(image, &mut result));
        } else {
            proof.profile = Some(self.ocr_profile(image, &mut result));
        }

        result.with_value(proof)
    }
}

const NAMES: [&str; 151] = [
  "Jambon-Beurre",
  "Bitter Jambon-Beurre",
  "Sweet Jambon-Beurre",
  "Salty Jambon-Beurre",
  "Sour Jambon-Beurre",
  "Spicy Jambon-Beurre",
  "Legendary Bitter Sandwich",
  "Legendary Salty Sandwich",
  "Legendary Sweet Sandwich",
  "Legendary Sour Sandwich",
  "Legendary Spicy Sandwich",
  "Jam Sandwich",
  "Great Jam Sandwich",
  "Ultra Jam Sandwich",
  "Master Jam Sandwich",
  "Peanut Butter Sandwich",
  "Great Peanut Butter Sandwich",
  "Ultra Peanut Butter Sandwich",
  "Master Peanut Butter Sandwich",
  "Pickle Sandwich",
  "Great Pickle Sandwich",
  "Ultra Pickle Sandwich",
  "Master Pickle Sandwich",
  "Marmalade Sandwich",
  "Great Marmalade Sandwich",
  "Ultra Marmalade Sandwich",
  "Master Marmalade Sandwich",
  "Herbed-Sausage Sandwich",
  "Great Herbed-Sausage Sandwich",
  "Ultra Herbed-Sausage Sandwich",
  "Master Herbed-Sausage Sandwich",
  "Curry-and-Rice-Style Sandwich",
  "Great Curry-and-Rice-Style Sandwich",
  "Ultra Curry-and-Rice-Style Sandwich",
  "Master Curry-and-Rice-Style Sandwich",
  "Dessert Sandwich",
  "Great Dessert Sandwich",
  "Ultra Dessert Sandwich",
  "Master Dessert Sandwich",
  "Tropical Sandwich",
  "Great Tropical Sandwich",
  "Ultra Tropical Sandwich",
  "Master Tropical Sandwich",
  "Avocado Sandwich",
  "Great Avocado Sandwich",
  "Ultra Avocado Sandwich",
  "Master Avocado Sandwich",
  "Noodle Sandwich",
  "Great Noodle Sandwich",
  "Ultra Noodle Sandwich",
  "Master Noodle Sandwich",
  "Potato Salad Sandwich",
  "Great Potato Salad Sandwich",
  "Ultra Potato Salad Sandwich",
  "Master Potato Salad Sandwich",
  "Zesty Sandwich",
  "Great Zesty Sandwich",
  "Ultra Zesty Sandwich",
  "Master Zesty Sandwich",
  "Egg Sandwich",
  "Great Egg Sandwich",
  "Ultra Egg Sandwich",
  "Master Egg Sandwich",
  "Classic Bocadillo",
  "Great Classic Bocadillo",
  "Ultra Classic Bocadillo",
  "Master Classic Bocadillo",
  "Refreshing Sandwich",
  "Great Refreshing Sandwich",
  "Ultra Refreshing Sandwich",
  "Master Refreshing Sandwich",
  "BLT Sandwich",
  "Great BLT Sandwich",
  "Ultra BLT Sandwich",
  "Master BLT Sandwich",
  "Fried Fillet Sandwich",
  "Great Fried Fillet Sandwich",
  "Ultra Fried Fillet Sandwich",
  "Master Fried Fillet Sandwich",
  "Ham Sandwich",
  "Great Ham Sandwich",
  "Ultra Ham Sandwich",
  "Master Ham Sandwich",
  "Cheese Sandwich",
  "Great Cheese Sandwich",
  "Ultra Cheese Sandwich",
  "Master Cheese Sandwich",
  "Hamburger Patty Sandwich",
  "Great Hamburger Patty Sandwich",
  "Ultra Hamburger Patty Sandwich",
  "Master Hamburger Patty Sandwich",
  "Smoky Sandwich",
  "Great Smoky Sandwich",
  "Ultra Smoky Sandwich",
  "Master Smoky Sandwich",
  "Fruit Sandwich",
  "Great Fruit Sandwich",
  "Ultra Fruit Sandwich",
  "Master Fruit Sandwich",
  "Variety Sandwich",
  "Great Variety Sandwich",
  "Ultra Variety Sandwich",
  "Master Variety Sandwich",
  "Klawf Claw Sandwich",
  "Great Klawf Claw Sandwich",
  "Ultra Klawf Claw Sandwich",
  "Master Klawf Claw Sandwich",
  "Sweet Sandwich",
  "Great Sweet Sandwich",
  "Ultra Sweet Sandwich",
  "Master Sweet Sandwich",
  "Vegetable Sandwich",
  "Great Vegetable Sandwich",
  "Ultra Vegetable Sandwich",
  "Master Vegetable Sandwich",
  "Hefty Sandwich",
  "Great Hefty Sandwich",
  "Ultra Hefty Sandwich",
  "Master Hefty Sandwich",
  "Five-Alarm Sandwich",
  "Great Five-Alarm Sandwich",
  "Ultra Five-Alarm Sandwich",
  "Master Five-Alarm Sandwich",
  "Nouveau Veggie Sandwich",
  "Great Nouveau Veggie Sandwich",
  "Ultra Nouveau Veggie Sandwich",
  "Master Nouveau Veggie Sandwich",
  "Spicy-Sweet Sandwich",
  "Great Spicy-Sweet Sandwich",
  "Ultra Spicy-Sweet Sandwich",
  "Master Spicy-Sweet Sandwich",
  "Decadent Sandwich",
  "Great Decadent Sandwich",
  "Ultra Decadent Sandwich",
  "Master Decadent Sandwich",
  "Tofu Sandwich",
  "Great Tofu Sandwich",
  "Ultra Tofu Sandwich",
  "Master Tofu Sandwich",
  "Curry-and-Noodle Sandwich",
  "Great Curry-and-Noodle Sandwich",
  "Ultra Curry-and-Noodle Sandwich",
  "Master Curry-and-Noodle Sandwich",
  "Tower Sandwich",
  "Great Tower Sandwich",
  "Ultra Tower Sandwich",
  "Master Tower Sandwich",
  "Sushi Sandwich",
  "Great Sushi Sandwich",
  "Ultra Sushi Sandwich",
  "Master Sushi Sandwich",
];
