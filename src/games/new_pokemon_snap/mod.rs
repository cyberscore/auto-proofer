// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;
use serde::{Deserialize, Serialize};

use crate::color::average_color_diff;
use crate::utils::load_image;
use crate::text::Text;

use image::GenericImageView;

#[wasm_bindgen(js_name = NPSProof)]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Proof {
    pub photo: Option<PhotoProof>,

    #[wasm_bindgen(getter_with_clone)]
    pub photodex: Option<PhotoDexProof>,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct PhotoProof {
    pub dex: usize,
    pub stars: usize,
    pub score: usize,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct PhotoDexProof {
    pub dex: usize,
    pub score: usize,
}

pub struct OCR {
    text: Text,
    text2: Text,
}

impl OCR {
    pub fn new() -> Self {
        let text = Text::new(
            &load_image(include_bytes!("train-nps.png")).unwrap(),
            128,
            &"0123456789,*".chars().collect::<Vec<_>>(), // ♂ is missing
        );

        let text2 = Text::new(
            &load_image(include_bytes!("train-nps-dex.png")).unwrap(),
            128,
            &"0123456789,".chars().collect::<Vec<_>>(),
        );

        Self { text, text2 }
    }

    pub fn ocr(&self, proof: &image::DynamicImage) -> Option<Proof> {
        let proof2;
        let proofr = if proof.dimensions() == (1920, 1080) {
            proof2 = proof.resize(1280, 720, image::imageops::FilterType::Lanczos3);
            &proof2
        } else {
            &proof
        };

        let is_photo_page = average_color_diff(proofr, 2, 541, 183, 102, (40, 40, 40)) < 20;
        let is_photodex = !is_photo_page;

        let mut result = Proof::default();

        let mut inverted = proofr.clone();
        inverted.invert();

        if is_photo_page {
            result.photo = Some(PhotoProof {
                dex: self.text2.read_usize_unlogged(&inverted, 700, 144, 55, 17, "0123456789,")?,
                stars: self.text.read_text(proofr, 188, 63, 170, 32, "*")?.chars().count(),
                score: self.text.read_usize_unlogged(proofr, 911, 194, 171, 43, "0123456789,")?,
            });
        } else if is_photodex {
            result.photodex = Some(PhotoDexProof {
                dex: self.text2.read_usize_unlogged(&inverted, 158, 23, 34, 22, "0123456789,")?,
                score: self.text.read_usize_unlogged(proofr, 1170, 70, 85, 27, "0123456789,")?,
            });
        }

        Some(result)
    }
}
