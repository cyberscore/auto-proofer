// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;
use serde::{Deserialize, Serialize};

use crate::color::test_average_color_diff;
use crate::utils::load_image;
use crate::characters::detect_characters;
use crate::text::{Text, Options};
use crate::ocr::{OCRable, Logged, BoundingBox, Logger};


#[wasm_bindgen(js_name = PLGProof)]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Proof {
    pub index: Option<IndexProof>,
    pub pokedex: Option<PokedexProof>,
    #[wasm_bindgen(getter_with_clone)] pub park: Option<ParkProof>,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct IndexProof {
    pub pokemon_obtained: usize,
    pub pokemon_seen: usize,
    pub total_pokemon_caught: usize,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct PokedexProof {
    pub dex: usize,
    pub alola: bool,
    pub number_caught: usize,
    pub metric: bool,
    pub weight_min: f64,
    pub weight_max: f64,
    pub height_min: f64,
    pub height_max: f64,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct ParkProof {
    #[wasm_bindgen(getter_with_clone)] pub dex: Vec<usize>,
    #[wasm_bindgen(getter_with_clone)] pub points: Vec<usize>,
    #[wasm_bindgen(getter_with_clone)] pub time: Vec<f64>,
}

fn range(min: Option<f64>, max: Option<f64>) -> (f64, f64) {
    if min.is_none() && max.is_none() {
        (0.0, 0.0)
    } else if min.is_none() {
        (max.unwrap(), max.unwrap())
    } else if max.is_none() {
        (min.unwrap(), min.unwrap())
    } else {
        (min.unwrap(), max.unwrap())
    }
}

fn parse_weight(text: &str, log: &mut dyn Logger) -> Option<f64> {
    if text == "---" {
        None
    } else {
        if text.contains('k') {
            log.log_plain(format!("is metric: {:?}", text));
            text.replace("'", ".").replace(",", ".")[0 .. text.len() - 2].parse::<f64>().ok()
        } else {
            log.log_plain(format!("is imperial: {:?}", text));
            text.replace("'", ".").replace(",", ".")[0 .. text.len() - 4].parse::<f64>().ok()
        }
    }
}

fn parse_height(text: &str, log: &mut dyn Logger) -> Option<f64> {
    lazy_static! {
        static ref RE: regex::Regex = regex::Regex::new(r#"^(\d+)'(\d{2})''$"#).unwrap();
    }

    log.log_plain(format!("parsing height: {}", text));

    if text == "---" {
        None
    } else {
        if text.contains('m') {
            log.log_plain(format!("is metric: {:?}", text));
            Some(text.replace("'", ".").replace(",", ".")[0 .. text.len() - 1].parse::<f64>().ok()? * 100.0)
        } else {
            log.log_plain(format!("is imperial: {:?}", text));
            let matches = RE.captures(text)?;

            let feet = matches.get(1).unwrap().as_str().parse::<f64>().ok()?;
            let inch = matches.get(2).unwrap().as_str().parse::<f64>().ok()?;

            Some(feet * 12.0 + inch)
        }
    }
}

fn parse_time(text: &str, log: &mut dyn Logger) -> Option<usize> {
    lazy_static! {
        static ref RE: regex::Regex = regex::Regex::new(r#"^(\d+)\.\.(\d{2})\.(\d{2})$"#).unwrap();
    }

    log.log_plain(format!("parsing time: {:?}", text));
    let matches = RE.captures(text)?;

    let m = matches.get(1).unwrap().as_str().parse::<usize>().ok()?;
    let s = matches.get(2).unwrap().as_str().parse::<usize>().ok()?;
    let c = matches.get(3).unwrap().as_str().parse::<usize>().ok()?;

    Some(m * 60 * 100 + s * 100 + c)
}

fn dex_from_name(text: &str) -> Option<usize> {
    let names = [
        "missingno",
        "bulbasaur", "ivysaur", "venusaur",
        "charmander", "charmeleon", "charizard",
        "squirtle", "wartortle", "blastoise",
        "caterpie", "metapod", "butterfree",
        "weedle", "kakuna", "beedrill",
        "pidgey", "pidgeotto", "pidgeot",
        "rattata", "raticate",
        "spearow", "fearow",
        "ekans", "arbok",
        "pikachu", "raichu",
        "sandshrew", "sandslash",
        "nidoran♀", "nidorina", "nidoqueen",
        "nidoran♂", "nidorino", "nidoking",
        "clefairy", "clefable",
        "vulpix", "ninetales",
        "jigglypuff", "wigglytuff",
        "zubat", "golbat",
        "oddish", "gloom", "vileplume",
        "paras", "parasect",
        "venonat", "venomoth",
        "diglett", "dugtrio",
        "meowth", "persian",
        "psyduck", "golduck",
        "mankey", "primeape",
        "growlithe", "arcanine",
        "poliwag", "poliwhirl", "poliwrath",
        "abra", "kadabra", "alakazam",
        "machop", "machoke", "machamp",
        "bellsprout", "weepinbell", "victreebel",
        "tentacool", "tentacruel",
        "geodude", "graveler", "golem",
        "ponyta", "rapidash",
        "slowpoke", "slowbro",
        "magnemite", "magneton",
        "farfetch'd",
        "doduo", "dodrio",
        "seel", "dewgong",
        "grimer", "muk",
        "shellder", "cloyster",
        "gastly", "haunter", "gengar",
        "onix",
        "drowzee", "hypno",
        "krabby", "kingler",
        "voltorb", "electrode",
        "exeggcute", "exeggutor",
        "cubone", "marowak",
        "hitmonlee", "hitmonchan",
        "lickitung",
        "koffing", "weezing",
        "rhyhorn", "rhydon",
        "chansey",
        "tangela",
        "kangaskhan",
        "horsea", "seadra",
        "goldeen", "seaking",
        "staryu", "starmie",
        "mrmime",
        "scyther",
        "jynx",
        "electabuzz",
        "magmar",
        "pinsir",
        "tauros",
        "magikarp", "gyarados",
        "lapras",
        "ditto",
        "eevee", "vaporeon", "jolteon", "flareon",
        "porygon",
        "omanyte", "omastar",
        "kabuto", "kabutops",
        "aerodactyl",
        "snorlax",
        "articuno", "zapdos", "moltres",
        "dratini", "dragonair", "dragonite",
        "mewtwo", "mew",
        "meltan", "melmetal",
    ];

    for (i, &name) in names.iter().enumerate() {
        if name == text {
            return Some(i);
        }
    }

    None
}

pub struct OCR {
    text: Text,
    text2: Text,
}

impl OCRable for OCR {
    type Proof = Proof;

    fn ocr(&self, image: &image::DynamicImage) -> Logged<Proof> {
        let mut result = Logged::new();

        result.log_plain("proof-type: checking".into());
        let is_index_page = test_average_color_diff(image, BoundingBox::new(0, 0, 127, 42), (74, 216, 240), 20, &mut result);
        let is_pokedex = test_average_color_diff(image, BoundingBox::new(8, 513, 100, 160), (12, 152, 212), 20, &mut result);
        let is_go_park = !is_pokedex && !is_index_page;

        let mut proof = Proof::default();

        if is_index_page { proof.index = Some(self.ocr_index_page(image, &mut result)); }
        if is_pokedex { proof.pokedex = Some(self.ocr_pokedex_entry(image, &mut result)); }
        if is_go_park { proof.park = Some(self.ocr_go_park(image, &mut result)); }

        result.with_value(proof)
    }
}

impl OCR {
    pub fn new() -> Self {
        let text = Text::new(
            &load_image(include_bytes!("train-plg.png")).unwrap(),
            50,
            &"0123456789.,'-abcdefghijklmnopqrstuvwxyz♀".chars().collect::<Vec<_>>(), // ♂ is missing
        );

        let text2 = Text::new(
            &load_image(include_bytes!("train-plg-light.png")).unwrap(),
            160,
            &"0123456789".chars().collect::<Vec<_>>(),
        );

        Self { text, text2 }
    }

    pub fn ocr_index_page(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> IndexProof {
        log.log_plain("proof-type: is index page".into());

        let mut proof = IndexProof::default();

        proof.pokemon_obtained = match self.text2.read_usize(image, Options::new().bbox(BoundingBox::new(357,   9,  75, 32)).chars("0123456789").log(log)) {
            Some(number) => number,
            None => { log.log_plain(format!("failed to read pokemon obtained")); return proof; },
        };

        proof.pokemon_seen = match self.text2.read_usize(image, Options::new().bbox(BoundingBox::new(773,   9,  75, 32)).chars("0123456789").log(log)) {
            Some(number) => number,
            None => { log.log_plain(format!("failed to read pokemon seen")); return proof; },
        };

        proof.total_pokemon_caught = match self.text2.read_usize(image, Options::new().bbox(BoundingBox::new(312, 685, 128, 32)).chars("0123456789,").log(log)) {
            Some(number) => number,
            None => { log.log_plain(format!("failed to read total pokemon caught")); return proof; },
        };

        proof
    }

    pub fn ocr_pokedex_entry(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> PokedexProof {
        log.log_plain("proof-type: is pokedex".into());

        let mut proof = PokedexProof::default();

        let mut inverted = image.clone();
        inverted.invert();

        let metric_text = self.text.read_text_logged(&inverted, BoundingBox::new(130, 302, 200, 40), "kglbs0123456789.", log);

        let is_metric = metric_text.contains("kg");
        let is_imperial = metric_text.contains("lbs");

        log.log(format!("is-metric: {}; is-imperial: {}", is_metric, is_imperial), BoundingBox::new(130, 302, 160, 40));
        if is_metric == is_imperial {
            log.log(format!("metric/imperial check mismatch"), BoundingBox::new(130, 302, 160, 40));
            return proof;
        }

        proof.metric = is_metric;

        let height_max = parse_height(&self.text.read_text_logged(&inverted, BoundingBox::new(1095, 184, 157, 43), "0123456789'.-m", log), log);
        let height_min = parse_height(&self.text.read_text_logged(&inverted, BoundingBox::new(1095, 221, 157, 43), "0123456789'.-m", log), log);
        (proof.height_min, proof.height_max) = range(height_min, height_max);

        let weight_max = parse_weight(&self.text.read_text_logged(&inverted, BoundingBox::new(1095, 287, 157, 43), "0123456789'.-lbskg", log), log);
        let weight_min = parse_weight(&self.text.read_text_logged(&inverted, BoundingBox::new(1095, 328, 157, 43), "0123456789'.-lbskg", log), log);
        (proof.weight_min, proof.weight_max) = range(weight_min, weight_max);

        proof.dex = match self.text.read_usize(&inverted, Options::new().bbox(BoundingBox::new(186, 39, 100, 40)).chars("0123456789").log(log)) {
            Some(number) => number,
            None => { log.log_plain(format!("failed to read dex number")); return proof; },
        };

        let alola_bbox = BoundingBox::new(24, 212, 261, 45);
        proof.alola = !detect_characters(&inverted, alola_bbox, 30).is_empty();
        log.log(format!("detecting alola: {}", proof.alola), alola_bbox);

        proof.number_caught = match self.text.read_usize(&inverted, Options::new().bbox(BoundingBox::new(1129, 70, 121, 34)).chars("0123456789,").log(log)) {
            Some(number) => number,
            None => { log.log_plain("failed to read number caught".into()); return proof; },
        };

        proof
    }

    pub fn ocr_go_park(&self, image: &image::DynamicImage, log: &mut dyn Logger) -> ParkProof {
        log.log_plain("proof-type: is go park".into());

        let mut proof = ParkProof::default();

        for x in 0..11 {
            log.log_plain(format!("reading go park entry {}", x));

            let points = match self.text.read_text_logged(&image, BoundingBox::new(720, 25 + 60 * x, 172, 30), "0123456789,", log).replace(',', "").parse::<usize>() {
                Ok(number) => number,
                Err(err) => { log.log_plain(format!("Couldn't read points: {:?}", err)); return proof; },
            };

            if points > 0 {
                let pokemon = self.text.read_text_logged(&image, BoundingBox::new(420, 25 + 60 * x, 200, 30), "abcdfeghijklmnopqrstuvwxyz♀♂", log).replace('.', "");
                log.log_plain(format!("pokemon name: {}", pokemon));

                let dex = match dex_from_name(&pokemon) {
                    Some(number) => number,
                    None => { log.log_plain(format!("Couldn't get dex number from name: {:?}", pokemon)); return proof },
                };

                let seconds = self.text.read_text_logged(&image, BoundingBox::new(1000, 25 + 60 * x, 172, 30), "0123456789,.", log);
                let seconds = match parse_time(&seconds, log) {
                    Some(number) => number as f64 / 100.0,
                    None => { log.log_plain(format!("Couldn't parse time: {:?}", seconds)); return proof },
                };

                proof.dex.push(dex);
                proof.points.push(points);
                proof.time.push(seconds);
            }
        }

        proof
    }
}
