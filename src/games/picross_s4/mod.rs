// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use wasm_bindgen::prelude::*;
use serde::{Deserialize, Serialize};

use crate::utils::load_image;
use crate::text::Text;

#[wasm_bindgen(js_name = PS4Proof)]
#[derive(
    Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq,
)]
pub struct Proof {
    #[wasm_bindgen(getter_with_clone)]
    pub score: Option<ScoreProof>,
}

#[wasm_bindgen]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct ScoreProof {
    #[wasm_bindgen(getter_with_clone)]
    pub section: String,

    #[wasm_bindgen(getter_with_clone)]
    pub level: String,
    pub seconds: usize,
}

pub struct OCR {
    text: Text,
}

impl OCR {
    pub fn new() -> Self {
        let text = Text::new(
            &load_image(include_bytes!("train-ps4.png")).unwrap(),
            128,
            &"apcxetroi.sl_m~g0123456789".chars().collect::<Vec<_>>(),
        );

        Self { text }
    }

    pub fn ocr(&self, proof: &image::DynamicImage) -> Option<Proof> {
        let mut inverted = proof.clone();
        inverted.invert();

        let section = self.text.read_text(&inverted, 373, 4, 538, 40, "clipormegas.")?.replace(".", "");

        let mut result = Proof::default();

        let mut score = ScoreProof::default();

        score.section = section.clone();
        if section == "clippicross" {
            score.level = self.text.read_text(proof, 293, 220, 182, 41, "cp_0123456789")?;

            let time = self.text.read_text(proof, 293, 333, 182, 41, "0123456789.")?;
            let mut parts = time.split("..");
            score.seconds = 3600 * parts.next()?.parse::<usize>().ok()?;
            score.seconds += 60 * parts.next()?.parse::<usize>().ok()?;
            score.seconds += parts.next()?.parse::<usize>().ok()?;
        } else {
            score.level = self.text.read_text(proof, 372, 531, 145, 59, "apcxetroi.sl_m~g0123456789")?;

            let time = self.text.read_text(proof, 703, 607, 282, 58, "0123456789.")?;
            let mut parts = time.split("..");
            score.seconds = 3600 * parts.next()?.parse::<usize>().ok()?;
            score.seconds += 60 * parts.next()?.parse::<usize>().ok()?;
            score.seconds += parts.next()?.parse::<usize>().ok()?;
        }

        result.score = Some(score);

        Some(result)
    }
}
