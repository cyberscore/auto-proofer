// SPDX-FileCopyrightText: 2022 Hugo Peixoto <hugo.peixoto@gmail.com>
// SPDX-License-Identifier: AGPL-3.0-only

use crate::characters;

#[derive(Debug)]
pub struct Signature {
    digit: characters::Digit,
}

impl Signature {
    pub fn new(digit: &characters::Digit) -> Self {
        Self { digit: digit.clone() }
    }

    pub fn compare(&self, other: &Self) -> usize {
        let mut best = 10000;
        for yo in -1..= 1 {
            for xo in -1..= 1 {
                let mut diff = 0;
                for y in 0 .. self.digit.bbox.h.max(other.digit.bbox.h) as i32 {
                    for x in 0 .. self.digit.bbox.w.max(other.digit.bbox.w) as i32 {
                        // TODO: this will underflow when xo/yo < 0.
                        // This is probably OK because `po` will be a ridiculous number and
                        // `contains` will return false, which is expected for OOB indices.
                        // I should fix this though, and maybe stop using usize for (x,y)
                        let tx = xo + x as i32;
                        let ty = yo + y as i32;

                        let ps = y * self.digit.bbox.w as i32 + x;
                        let po = ty * other.digit.bbox.w as i32 + tx;

                        if self.digit.pointset.contains(ps as usize) != other.digit.pointset.contains(po as usize) {
                            diff += 1;
                        }
                    }
                }
                best = best.min(diff);
            }
        }

        best
    }
}
